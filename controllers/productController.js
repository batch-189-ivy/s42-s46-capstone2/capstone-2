const Product = require('../models/Product');
const bcrypt = require('bcrypt');
const auth = require('../auth')


// Create a product
module.exports.addProduct = (reqBody) => {
	let newProduct = new Product({
		productImage: reqBody.productImage,
		productName: reqBody.productName,
		description: reqBody.description,
		price: reqBody.price

	})
	return newProduct.save().then((product, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})
}

//Retrieve active products
module.exports.getActiveProducts = (data) => {

	return Product.find({isActive: true}).then(result => {
		return result
	})
}

//Retrieve All Products
module.exports.getAllProducts = async (data) => {
	
	if (data.isAdmin) {
		return Product.find({}).then(result => {
		return result
	})

	} else {
		return false
	}
}


module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {
		return result
	})
}

module.exports.updateProduct = (reqParams, reqBody) => {

	let updatedProduct = {
		productName: reqBody.productName,
		description: reqBody.description,
		price: reqBody.price
	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, err) => {
		if(err) {
			return false
		} else {
			return true
		}
	})
}

module.exports.archiveProduct = (data) => {

	return Product.findByIdAndUpdate(data.productId).then((result, err) => {

		if(data.payload === true) {
			result.isActive = false 

			return result.save().then((archivedProduct, err) => {
				if(err) {
					return false
				} else {
					return true
				}
			})
		} else {
			return false
		}
	})
}

module.exports.reactivateProduct = (data) => {

	return Product.findByIdAndUpdate(data.productId).then((result, err) => {

		if(data.payload === true) {
			result.isActive = true 

			return result.save().then((archivedProduct, err) => {
				if(err) {
					return false
				} else {
					return true
				}
			})
		} else {
			return false
		}
	})
}


