
const User = require('../models/User');
const Product = require('../models/Product');
const CartOrders = require('../models/CartOrders');
const bcrypt = require('bcrypt');
const auth = require('../auth')

//Check existing email
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if (result.length > 0) {
			return true
		} else {
			return false
		}
	})
}


//User Registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		mobileNo: reqBody.mobileNo,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user, error) => {
		if (error) {
			return false
		} else {
			return true
		}
	})
}

//User Authentication
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return false
		} else {
			const isPasswordCorrect = bcrypt. compareSync(
				reqBody.password, result.password)

			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}

//Retrieve a user
module.exports.getProfile = (data) => {
	return User.findById(data.id).then(result => {


		if (result == null) {
			return false
		} else {
			result.password = ""

			return result
		}
	})
}




//Set as Admin
module.exports.setAsAdmin = (reqParams, reqBody) => {

	let updatedAdmin = {
		isAdmin: reqBody.isAdmin
	}

	return User.findByIdAndUpdate(reqParams.userId, updatedAdmin).then((user, err) => {
		if(err) {
			return false
		} else {
			return true
		}
	})
}

/*//Retrieve user
module.exports.getProfile = (data) => {
	return User.findById(data.id).then(result => {

		//console.log(result)

		if (result == null) {
			return false
		} else {
			
			return true
		}
	})
}*/


//Retrieve all users - admin only
module.exports.getAllProfile = (data) => {
	return User.find({}).then(result => {

		//console.log(result)

		if (result == null) {
			return false
		} else {
			result.password = ""

			return result
		}
	})
}












//Add to Cart
module.exports.addToCart = (data) => {
	return User.findById(data.userId).then(result => {
		return Product.findById(data.productId).then(product => {
			
			result.cart.push({
			productId: data.productId,
			productImage: data.productImage,
			productName: data.productName,
			quantity: data.quantity,
			price: data.price,
			subTotal: data.subTotal
		})


		return result.save().then((user, error) => {
			if(error){
				return false
			} else {
				return true
			}
		})
	})
})

}



//remove items from cart
module.exports.removeItems = (data) => {
	return User.findById(data.userId).then(user => {
		if(user.cart.length > -1) {
			user.cart.splice(0)
			return user.save().then((user, error) => {

			if(error) {
				return false
			} else {
				return true
			}
		})

 
		}  
	})
}



//get all carts - admin only
/*module.exports.getAllCarts = async (data) => {
	return CartOrders.find().then(result => {

			if(result == null) {
				return "Carts are empty"
			} else {
				return result
			}

		
	})
}
*/

module.exports.getAllCarts = async (data) => {
	let checkCarts = await User.find().then(users => {

			let CartOrders = []


		    for (let i = 0; i < users.length; i++) {

				if(users[i].cart.length !== 0) {
					CartOrders.push({userId: users[i].id, cart: users[i].cart})
					continue
				} else {
					continue;
				}
		}

		return CartOrders
		
	}).then(result => {
		return result
	})

	let showAllCarts = await CartOrders.find().then(result => {
			if (result == null) {
				return false
			} else {
				return result

				console.log(result)
			}
	})

}





//get user cart - non admin
module.exports.getUserCart = (data) => {

	return User.findById(data.id).then(result => {
		if(result == null) {
			return "Cart is empty"
		}  else {
			return result.cart
		}
	})
}


//Checkout 
module.exports.checkOutCart = async (data) => {

	let userOrder = await User.findById(data.userId).then((user, error) => {

		if(error) {
			return error
		} else {
			return user.cart
		}

	})

	if (userOrder !== null ) {

		let newCartOrders = new CartOrders ({
		userId: data.userId,
		checkOut: userOrder
		})



	return newCartOrders.save().then((result, error) => {
		if(error) {
			return error
		} else {
			return true
		}
	})

	} else {
		return false
	}






	/*let initialValue = 0
	let total = data.reduce((add, current) => add + current.price * current.quantity, initialValue)*/


	
}



