const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require('../auth')


//Create a product
router.post("/create", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	const isAdminData = userData.isAdmin

	let data = {
		productImage: req.body.productImage,
		productName: req.body.productName,
		description: req.body.description,
		price: req.body.price
	}

	if(isAdminData) {
		productController.addProduct(data).then(resultFromController => res.send(resultFromController))
	} else {
		res.send("User is not an admin")
	}

});

router.get("/", (req, res) => {
	productController.getActiveProducts().then(resultFromController => res.send(resultFromController))
});


//Route for retrieving all the courses
router.get("/allproducts", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	productController.getAllProducts(userData).then(resultFromController => res.send(resultFromController))
});


router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
});

router.put("/update/:productId", auth.verify, (req, res) => {
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
});

router.put('/:productId/archive', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)


	let data = {
		productId: req.params.productId,
		payload: auth.decode(req.headers.authorization).isAdmin
	}

	productController.archiveProduct(data).then(resultFromController => res.send(resultFromController))
})


//Reactivate a product
router.put('/:productId/reactivate', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)


	let data = {
		productId: req.params.productId,
		payload: auth.decode(req.headers.authorization).isAdmin
	}

	productController.reactivateProduct(data).then(resultFromController => res.send(resultFromController))
})


module.exports = router;
