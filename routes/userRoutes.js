const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth')


//Check existing email
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

//Register a user
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})


//User login
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})

//Retrieve user - non admin

router.get("/details", auth.verify, (req, res) => {
	//Provides the user's ID for the getProfile controller method

	const userData = auth.decode(req.headers.authorization);
	


	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
})


//Retrieve all users - admin only
router.get("/alldetails", auth.verify, (req, res) => {
	//Provides the user's ID for the getProfile controller method

	const userData = auth.decode(req.headers.authorization);
	const isAdminData = auth.decode(req.headers.authorization).isAdmin

	if(isAdminData === true) {
		userController.getAllProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
	} else {
		return false
	}
	
})



//Change isAdmin details
router.put('/:userId/setAdmin', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	const isAdminData = userData.isAdmin

	if(isAdminData) {

//Argument 1. ID of the task to be updated. 2. New content of the task
		userController.setAsAdmin(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send("User is not an admin")
	}
})

//add to cart
/*router.post("/cart", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	const isAdmin = userData.isAdmin


	let data = {
		userId: userData.id,
		productId: req.body.productId,
		productName: req.body.productName,
		quantity: req.body.quantity,
		price: req.body.price
	}

	if(isAdmin == false) {
		userController.addToCart(data).then(resultFromController => {
		res.send(resultFromController)
	})
	} else {
		return "User is an admin!"
	}
})*/







//add to cart
router.post("/addtocart/", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	const isAdminData = userData.isAdmin

	let data = {
		userId: userData.id,
		productImage: req.body.productImage,
		productId: req.body.productId,
		productName: req.body.productName,
		quantity: req.body.quantity,
		price: req.body.price
	}
	if(isAdminData == false) {
	userController.addToCart(data).then(resultFromController => res.send(resultFromController))
	} else {
		return "User is an admin!"
	}


});


//update items
/*router.put("/mycart", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	let data = {
		userId: userData.id,
		productImage: req.body.productImage,
		productId: req.body.productId,
		productName: req.body.productName,
		quantity: req.body.quantity,
		price: req.body.price
	}

	userController.updateCart(userData.id, data).then(resultFromController => res.send(resultFromController))

})*/


router.delete("/removeItems/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	const isAdminData = userData.isAdmin

	let data = {
		userId: userData.id
	}

	if(isAdminData == false) {
	userController.removeItems(data).then(resultFromController => res.send(resultFromController))
	} else {
		return "User is an admin!"
	}
});



//addto cart
/*router.post("/cart", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	const isAdminData = userData.isAdmin

	let data = {
		userId: userData.id,
		productImage: req.body.productImage,
		productId: req.body.productId,
		productName: req.body.productName,
		quantity: req.body.quantity,
		price: req.body.price
	}
	if(isAdminData == false) {
	userController.addToCart(data).then(resultFromController => res.send(resultFromController))
	} else {
		return "User is an admin!"
	}


});*/

//Edit cart
/*router.put("/mycart", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	const isAdminData = userData.isAdmin

	let data = {
		userId: userData.id,
		productImage: req.body.productImage,
		productId: req.body.productId,
		productName: req.body.productName,
		quantity: req.body.quantity,
		price: req.body.price
		
	}
	if(isAdminData == false) {
	userController.updateCart(data).then(resultFromController => res.send(resultFromController))
	} else {
		return "User is an admin!"
	}


});*/


//get all carts - admin only
router.get("/allorders", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	const isAdminData = userData.isAdmin

	if(isAdminData) {
		userController.getAllCarts().then(resultFromController => res.send(resultFromController))
	} else {
		return false
	}
});
	




//get user cart - non admin
router.get("/mycart" , auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	const isAdminData = userData.isAdmin

	let data = {
		userId: userData.id
	}

	if (isAdminData === false) {
		userController.getUserCart({id: userData.id}).then(resultFromController => res.send(resultFromController))
	} else {
		return false
	}
	
});

//checkout
router.post("/mycart/checkout", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	const isAdminData = userData.isAdmin

	let data = {
		userId: userData.id
	}

	if (isAdminData === false) {
		userController.checkOutCart(data).then(resultFromController => res.send(resultFromController))
	} else {
		return false
	}
});


module.exports = router;
