/*const express = require('express');
const router = express.Router();
const User = require('../models/User');
const Cart = require('../models/Cart');
const cartController = require('../controllers/cartController');
const auth = require('../auth');


//create a cart
router.post("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	const isAdmin = userData.isAdmin

	

	if(isAdmin == false) {
		cartController.createCart({id: userData.id}).then(resultFromController => {
		res.send(resultFromController)
	})
	} else {
		return "User is an admin!"
	}


})


//add items to cart
router.post("/addtocart", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	const isAdminData = userData.isAdmin


	let data = {
		userId: userData.id,
		productId: req.body.productId,
		productName: req.body.productName,
		quantity: req.body.quantity,
		price: req.body.price
	}

	if(isAdminData == false) {
		cartController.addToCart(data).then(resultFromController => {
		res.send(resultFromController)
	})
	} else {
		return "User is an admin!"
	}
})




//get all carts - admin only
router.get("/all", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	cartController.getAllCarts(userData).then(resultFromController => res.send(resultFromController))
})



//get user cart - non admin
router.get("/:cartId" , auth.verify, (req, res) => {

	cartController.getUserCart(req.params).then(resultFromController => res.send(resultFromController))
})





module.exports = router;*/