const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({

	productImage: {
		type: String
	},
	productName:{
		type: String,
		required: [true, "Product name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	isActive: {
		type: Boolean,
		default:true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	userDetails: 
		[{
			userId: {
				type: String,
				required: [true, "User Id is required"]
			},
			boughtOn: {
				type: Date,
				default: new Date()
			}
		}]

})

module.exports = mongoose.model("Product", productSchema)
