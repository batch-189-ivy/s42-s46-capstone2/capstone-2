const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile number is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	cart: [{
			userId: {
				type: String
			},
			productId: {
				type:String
			},
			productImage: {
				type: String
			},
			productName:  {
				type:String
				
			},
			quantity:  {
				type:Number
			},
			price: {
				type:Number
			},
			subTotal: {
				type: Number
			},
			orderedOn:{
			type: Date,
			default: new Date()
			}
	
		}
	]



})

module.exports = mongoose.model("User", userSchema)

